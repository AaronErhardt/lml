## The concept

Books have been the main source of information for centuries. Even today this hasn’t changed although screens have replaced paper in many cases. Reading is surely one of the best methods to learn yet it could be improved. Our modern computers and smart devices allow us to learn more interactively that ever before and still this opportunity is rarely used. Resources shouldn't be just packed with data but should encourage the reader to recap and deepen information. LML was created to improve the quality of online resources and learning in general by providing a simple yet powerful language.

## What can LML do?

LML is a language to create online courses. A course contains multiple chapters and chapters contain several lessons. A course can be opened as a website or embedded into a website. An example of a course can be found on this site: https://aaronerhardt.gitlab.io/lml/


## Advantages of LML

- Fast to learn
- Easy to use - it's hardly more than writing the text
- Extensible
- Available on Smartphones and PC
- Free and open source


## Getting started

1. Download this repository
2. Extract the files. You will only need the ``public`` folder
3. General settings can be edited in the ``lml.json`` file.
4. To add a new chapter create a folder with the specified chapter name and the number of the chapter at the end. To add a new lesson create new file and name it with the specified lesson name, its number and the ``.lml`` file ending.
5. Update the ``lml.json`` file so the parser knows which chapters and lessons exist.
6. Open the ``index.html`` file in your browser to load your course.
7. Deploy a course by simply uploading the ``public`` folder to a web server.
> A tutorial for LML will be added soon, written in LML itself


## Version

LML can already be used for online courses and works quite flawless. Yet many new features and extensions are planned, also your feedback would help me a lot. Should you notice a bug, crash, spelling mistake or anything related or should you have an idea for new features or improvements let me know it! Simply contact me at aaron.erhardt@t-online.de.
