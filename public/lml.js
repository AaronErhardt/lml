/*
Copyright (C) 2018-2019 Aaron Erhardt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

"use strict"

/* Global variables */

const lml = {
  chapter: 1,
  lesson: 1,
  code: "",
  root: {},
  htmlRoot: {},
  course: {},
  cdn: ""
};

const attribs = {
  bool: ["br", "correct", "hide"],
  certain: [{
      name: "lang",
      values: ["c", "arduino"]
    },
    {
      name: "type",
      values: ["select", "sort", "match"]
    },
  ],
  free: ["src", "url"]
};

const qObj = (o) => {

  if (o === undefined || o === null) {
    util.print("DEBUG: Unknown object!");
    return undefined;
  }

  if (o.ev !== undefined) {
    return o;
  }

  o.ev = [];

  o.aEv = (ev, func, capt = false) => o.addEventListener(ev, func, capt);

  o.rEv = (ev, func, capt = false) => {

    if (ev === undefined) {
      for (let i = o.ev.length - 1; i >= 0; i--) {
        o.removeEventListener(o.ev[i].name, o.ev[i].func, o.ev[i].capt);
      }
      o.ev = [];

    } else if (func === undefined) {

      for (let i = o.ev.length - 1; i >= 0; i--) {
        if (ev === o.ev[i].name) {
          o.removeEventListener(ev, o.ev[i].func, o.ev[i].capt);
          o.ev.splice(i, 1);
          return;
        }
      }

    } else if (typeof(func) === "number") {
      o.removeEventListener(ev, o.ev[func], ev[func].capt);
      o.ev.splice(func, 1);

    } else {

      o.removeEventListener(ev, func, capt);
    }
  };

  o.cEv = (ev, func, capt = false) => {
    o.ev.push({
      name: ev,
      func: func,
      capt: capt
    });
    o.addEventListener(ev, func, capt);
  };

  //Element queries\\

  o.Q = (q) => {
    return qObj(o.querySelector(q));
  };

  o.A = (q) => {
    let t = o.querySelectorAll(q);
    let c = [];

    for (let i = t.length - 1; i >= 0; i--) {
      c[i] = qObj(t[i]);
    }
    return c;
  };

  //DOM operations\\

  o.aChild = (c) => {
    return qObj(o.appendChild(c));
  };

  o.rChild = (c) => {
    return o.removeChild(c);
  };

  return o;
};

//vanilla

const w = qObj(window);

const d = qObj(document);

d.cElem = (e, q = true) => {
  return qObj(d.createElement(e));
};

d.cText = (t) => {
  return d.createTextNode(t);
};

const V = (q) => {
  return d.querySelector(q);
};

const Y = (q) => {
  return d.querySelectorAll(q);
};

//Qute

const Q = (q) => {
  return qObj(d.querySelector(q));
};

const A = (q) => {
  let t = d.querySelectorAll(q);
  let c = [];

  for (let i = t.length - 1; i >= 0; i--) {
    c[i] = qObj(t[i]);
  }
  return c;
};

const load = {

  lesson: (chapter, lesson) => {
    let path = lml.course.folderName + lml.course.chapterName + chapter + "/" + lml.course.lessonName + lesson + ".lml";
    lml.chapter = chapter;
    lml.lesson = lesson;

    let func = (ev) => {
      lml.code = ev.target.responseText;

      util.clear();
      parse();
    }

    load.document(path, func);
    w.scrollTo(0, 0);
  },

  overview: () => {
    util.clear();

    let node = lml.htmlRoot,
      chapterNum = 1;

    let html = d.cElem("div");
    html.className = "lml-overview";
    node = node.appendChild(html);

    html = d.cElem("h");
    html.textContent = lml.course.name;
    html.className = "lml-course-name";
    node.aChild(html);

    html = d.cElem("p");
    html.textContent = lml.course.description;
    html.className = "lml-course-description";
    node.aChild(html);

    for (let chapter of lml.course.chapter) {
      html = d.cElem("div");
      html.className = "lml-chapter";
      let cNode = node.aChild(html);

      let evChapter = chapterNum;
      cNode.aEv("click", () => {
        load.lesson(evChapter, 1);
      });

      html = d.cElem("h");
      html.textContent = chapter.name;
      html.className = "lml-chapter-name";
      cNode.aChild(html);

      html = d.cElem("p");
      html.textContent = chapter.description;
      html.className = "lml-chapter-description";
      cNode.aChild(html);

      ++chapterNum;
    }
    w.scrollTo(0, 0);
  },

  document: (path, func) => {
    util.print("opening document at " + path);

    let client = new XMLHttpRequest();
    client.responseType = "text";
    client.open('GET', path, true);

    let evFunc = (ev) => {
      if (ev.target.readyState === 4) {
        if (ev.target.status === 200) {
          func(ev);
        } else {
          console.error("couldn't open document at '" + path + "' (" + ev.target.status + ")");
        }
      }
    };
    client.addEventListener("readystatechange", evFunc);
    client.send();
  },

  css: (path) => {
    let head = document.getElementsByTagName("head")[0];
    let link = d.cElem("link");
    link.rel = "stylesheet";
    link.type = "text/css";
    link.href = lml.cdn + path;
    head.appendChild(link);
  }
};

const util = {
  fixJson() {
    if (lml.course.folderName === undefined)
      lml.course.folderName = "";
    if (lml.course.chapterName === undefined)
      lml.course.chapterName = "CHAPTER_";
    if (lml.course.lessonName === undefined)
      lml.course.lessonName = "LESSON_";

    if (lml.course.meta === undefined)
      lml.course.meta = {};
    if (lml.course.meta.word === undefined)
      lml.course.meta.word = {};
    if (lml.course.meta.lang === undefined)
      lml.course.meta.lang = "en";
    if (lml.course.meta.word.back === undefined)
      lml.course.meta.word.back = "go back";
    if (lml.course.meta.word.submit === undefined)
      lml.course.meta.word.submit = "submit";
    if (lml.course.meta.word.continue === undefined)
      lml.course.meta.word.continue = "continue";
    if (lml.course.meta.word.skip === undefined)
      lml.course.meta.word.skip = "skip";
    if (lml.course.meta.word.skipLesson === undefined)
      lml.course.meta.word.skipLesson = "skip lesson";
    if (lml.course.meta.word.again === undefined)
      lml.course.meta.word.again = "try again";
  },

  clear: () => {
    if (lml.htmlRoot.firstChild !== null)
      lml.htmlRoot.rChild(lml.htmlRoot.firstChild);
  },

  getTagName: (tag) => {
    let tagNameSize = tag.length;
    for (let i = 0; i < tagNameSize; i++) {
      if (tag.charAt(i) === " ")
        return tag.substr(0, i);
    }
    return tag;
  },

  cutEdges: (str) => {
    let length = str.length;
    let begin = 0,
      end = length;

    for (let i = 0; i < length; i++) {
      let char = str.charAt(i);
      if (char !== ' ' && char !== '\n') {
        begin = i;
        break;
      }
    }

    for (let i = length - 1; i >= 0; i--) {
      let char = str.charAt(i);
      if (char !== ' ' && char !== '\n') {
        end = i + 1;
        break;
      }
    }
    return str.substr(begin, end - begin);
  },

  print: (str) => {
    if (lml.course.showLog === true)
      console.log(str);
  }
};

const lesson = {
  next: () => {
    if (lml.course.chapter[lml.chapter - 1].lesson.length === lml.lesson)
      load.overview();
    else
      load.lesson(lml.chapter, lml.lesson + 1);
  },

  last: () => {
    if (lml.lesson <= 1)
      load.overview();
    else
      load.lesson(lml.chapter, lml.lesson - 1);
  }
};

const html = {
  addTag: (obj, tagName) => {
    let htmlTag = d.cElem(tagName);
    htmlTag.className = "lml-" + obj.tagName;
    obj.html = obj.parent.html.appendChild(htmlTag);

    for (let elem of obj.children) {
      elem.aHTML();
    }
  },

  addSpan: (obj, str) => {
    let htmlTag = d.cElem("span");
    let t_node;
    if (str === undefined) {
      t_node = document.createTextNode(obj.text);
    } else {
      t_node = document.createTextNode(str);
    }
    obj.html = obj.parent.html.appendChild(htmlTag);
    obj.html.className = "lml-" + obj.tagName;
    obj.html.appendChild(t_node);
  }
};

// Objects

class tagType {
  constructor(tag) {
    this.parent;
    this.children = [];
    this.tagName = tag;
    this.html;
    this.tagInterupt = true;
    this.lastChild;
    this.textSave = false;
    this.childrenNames = [];
    util.print("new TAG '" + tag + "' created");
  }

  aChild(tag) {
    if (this.childrenNames.includes(tag)) {
      let length = this.children.length;
      this.children[length] = new_node(tag);
      this.lastChild = this.children[length];
      util.print("new CHILD '" + this.lastChild.tagName + "' of '" + this.tagName + "'");
      this.lastChild.parent = this;
    } else
      console.error("Adding child '" + tag + "' to '" + this.tagName + "' is not allowed");
  }

  aHTML() {
    html.addTag(this, "div");
  }
}

const tag = {

  b: class extends tagType {
    constructor() {
      super("b");
      this.childrenNames = ["string"];
      this.textSave = true;
    }
    aHTML() {
      html.addSpan(this, this.lastChild.text);
    }
  },

  back: class extends tagType {
    constructor() {
      super("back");
      this.active = false;
    }
    aHTML() {
      let htmlTag = d.cElem("p");
      htmlTag.className = "lml-back";

      htmlTag.innerText = lml.course.meta.word.back;

      this.html = this.parent.html.appendChild(htmlTag);

      this.html.aEv("click", () => {
        if (this.html === V(".lml-back")) {
          lesson.last();
        } else {
          let lml = this.parent;
          while (lml.tagName !== "lml" && lml.parent !== undefined)
            lml = lml.parent;
          let node = lml.miniCache[this.parent.minifyNumber - 1];
          node.className = node.className.substr(0, node.className.length - 13);
          this.parent.html.className += " lml-minified";
        }
      });
    }
  },

  box: class extends tagType {
    constructor() {
      super("box");
      this.childrenNames = ["option"];
      this.type = "select";
      this.dragged = undefined;
    }

    swap(first, second) {
      first.className = "lml-option";
      second.className = "lml-option lml-dragged";

      let order = second.order;
      second.order = first.order;
      first.order = order;

      this.dragged = second;
      let oldNode = first.replaceChild(second.childNodes[1], first.childNodes[1]);
      second.appendChild(oldNode);

      util.print("swapped! " + first.num + " " + second.num);
    }

    get check() {
      switch (this.type) {
        case "sort":

          for (let elem of this.children) {
            if (elem.html.num !== elem.html.order) {
              this.html.style.boxShadow = "0 0 0.2rem 0.05rem var(--lml-red)";
              return false;
            }
          }
          this.html.style.boxShadow = "0 0 0.2rem 0.05rem var(--lml-green)";
          return true;

          break;
          /*case "match": // will be added soon

            break;*/
        default: // = select

          for (let elem of this.children) {
            for (let child of elem.html.children) {
              if ((child.className === "lml-select" && elem.correct === true) || (child.className === "lml-select active" && elem.correct === false)) {
                this.html.style.boxShadow = "0 0 0.2rem 0.05rem var(--lml-red)";
                return false;
              }

            }
          }
          this.html.style.boxShadow = "0 0 0.2rem 0.05rem var(--lml-green)";
          return true;
          break;
      }
      this
    }

    aHTML() {
      let htmlTag = d.cElem("div");
      htmlTag.className = "lml-box";
      this.html = this.parent.html.aChild(htmlTag);

      if (this.type === "sort") {

        let length = this.children.length;
        for (let i = 0; i < length;) {
          let rand = Math.floor(Math.random() * length);
          if (this.children[rand].html === undefined) {
            this.children[rand].aHTML(rand, i);
            i++;
          }
        }

        this.html.aEv("dragend", (ev) => {
          let target = ev.target;

          if (!target.className.includes("lml-option"))
            target = target.parentNode;

          V(".lml-dragged").className = "lml-option";

          this.dragged = undefined;

          //util.print("dragend");
        });

        this.html.aEv("drop", (ev) => {
          ev.preventDefault();
        });

        this.html.aEv("dragstart", (ev) => {
          let target = ev.target;

          if (!target.className.includes("lml-option"))
            target = target.parentNode;

          target.className = "lml-option lml-dragged";

          ev.dataTransfer.setData("text", target.id);

          this.dragged = target;

          //util.print("dragstart");
        });
      } else {
        for (let elem of this.children) {
          elem.aHTML();
        }
      }
    }
  },

  br: class extends tagType {
    constructor() {
      super("br");
      this.tagInterupt = false;
    }
    aHTML() {
      html.addTag(this, "br");
    }
  },

  code: class extends tagType {
    constructor() {
      super("code");
      this.childrenNames = ["string", "br"];
      this.src = "";
      this.br = "true";
      this.lang = "";
      this.textSave = true;
    }
    aHTML() {
      if (this.lang !== "") {
        let htmlTag = d.cElem("pre");
        this.html = this.parent.html.appendChild(htmlTag);
        this.html.className = "lml-" + this.tagName;
        for (let elem of this.children) {
          elem.aHTML();
        }
        for (let elem of this.children) {
          if (elem.tagName = "p") {
            highlight(util.cutEdges(elem.text), this.lang, elem.html);
          }
        }
      } else if (this.br === "true") {
        for (let i = 0; i < this.children.length; i++) {
          if (this.children[i].tagName === "string") {
            this.children[i].text = util.cutEdges(this.children[i].text);
          }
        }
        html.addTag(this, "pre");
      } else {
        html.addTag(this, "div");
      }
      return;
    }
  },

  command: class extends tagType {
    constructor() {
      super("command");
      this.childrenNames = ["string"];
      this.textSave = true;
    }
    aHTML() {
      html.addSpan(this, this.lastChild.text);
    }
  },

  root: class extends tagType {
    constructor() {
      super("root");
      this.childrenNames = ["lml"];
    }

    aHTML(start) {
      this.html = lml.htmlRoot;

      for (let elem of this.children) {
        elem.aHTML();
      }
    }
  },

  h1: class extends tagType {
    constructor() {
      super("h1");
      this.childrenNames = ["string", "br"];
    }

    aHTML() {
      html.addTag(this, "div");
    }
  },

  h2: class extends tagType {
    constructor() {
      super("h2");
      this.childrenNames = ["string", "br"];
    }

    aHTML() {
      html.addTag(this, "div");
    }
  },

  h3: class extends tagType {
    constructor() {
      super("h3");
      this.childrenNames = ["string", "br"];
    }

    aHTML() {
      html.addTag(this, "div");
    }
  },

  i: class extends tagType {
    constructor() {
      super("i");
      this.childrenNames = ["string"];
      this.textSave = true;
    }
    aHTML() {
      html.addSpan(this, this.lastChild.text);
    }
  },

  image: class extends tagType {
    constructor() {
      super("image");
      this.tagInterupt = false;
      this.src = "";
    }

    aHTML() {
      if (this.src === "") {
        console.error("no image source defined");
        return;
      }
      let htmlTag = d.cElem("img");
      htmlTag.className = "lml-image";
      htmlTag.src = this.src;
      this.html = this.parent.html.appendChild(htmlTag);
    }
  },

  input: class extends tagType {
    constructor() {
      super("input");
    }

    aHTML() {
      html.addTag(this, "input");
      this.html.type = "text";
      this.html.value = this.text;
      this.html.size = this.text.length;
    }
  },

  lesson: class extends tagType {
    constructor() {
      super("lesson");
      this.childrenNames = ["h1", "h2", "h3", "p", "code", "string", "image", "submit", "back"];
    }

    aHTML() {
      html.addTag(this, "div");
    }
  },

  link: class extends tagType {
    constructor() {
      super("link");
      this.childrenNames = ["string"];
      this.url = "";
    }

    aHTML() {
      html.addTag(this, "a");

      if (this.url === "")
        console.error("link url is not defined!");
      else
        this.html.href = this.url;
    }
  },

  lml: class extends tagType {
    constructor(tag) {
      super("lml");
      this.childrenNames = ["lesson", "quiz", "seperator", "submit", "back"];
      this.hide = false;
    }

    aHTML() {
      if (!this.hide) {
        this.aChild("submit");
        this.aChild("back");
      }

      let htmlTag = d.cElem("div");

      htmlTag.className = "lml-lml";
      this.html = this.parent.html.appendChild(htmlTag);

      if (this.hide) {

        let length = this.children.length;
        for (let i = 0; i < length; i++) {}

        let count = 0,
          mini = false;
        this.miniCache = [];

        for (let elem of this.children) {
          if (elem.tagName !== "seperator") {
            elem.aChild("submit");
            elem.aChild("back");

            elem.aHTML();
            elem.minifyNumber = count;
            this.miniCache[count] = elem.html;
            elem.html.className += " lml-buttons";
            count++;

          } else {
            elem.aHTML();
          }

          if (elem.tagName !== "seperator") {
            if (mini)
              elem.html.className += " lml-minified";
            else
              mini = true;
          }
        }
        this.miniCache[count - 1].id = "lml-lastMinified";

      } else {
        this.html.className += " lml-buttons";

        for (let elem of this.children) {
          elem.aHTML();
        }
      }
    }
  },

  m: class extends tagType {
    constructor() {
      super("m");
      this.childrenNames = ["string"];
      this.textSave = true;
    }
    aHTML() {
      html.addSpan(this, this.lastChild.text);
    }
  },

  option: class extends tagType {
    constructor() {
      super("option");
      this.correct = false;
      this.textSave = "true";
      this.childrenNames = ["string"];
    }

    aHTML(order, num) {
      if (this.parent.type === "sort") {

        let htmlTag = d.cElem("div");
        htmlTag.className = "lml-option";
        htmlTag.draggable = true;
        this.html = this.parent.html.appendChild(htmlTag);
        this.html.num = num;
        this.html.order = order;

        let node = d.cElem("p");
        let text = d.cText(num + 1);
        node.append(text);
        node.className = "lml-option-num";
        this.html.appendChild(node);

        for (let elem of this.children) {
          elem.aHTML();
        }

        this.html.aEv("dragover", (ev) => {
          ev.preventDefault();

          let target = ev.currentTarget;

          if (this.parent.dragged.num !== target.num) {
            this.parent.swap(this.parent.dragged, target);
          }
        });

      } else if (this.parent.type === "match") {

      } else {
        let htmlTag = d.cElem("div");
        htmlTag.className = "lml-option";
        this.html = this.parent.html.appendChild(htmlTag);

        htmlTag = d.cElem("div");
        htmlTag.className = "lml-select";
        this.html.appendChild(htmlTag);

        for (let elem of this.children) {
          elem.aHTML();
        }

        this.html.aEv("click", (ev) => {
          let opt = ev.currentTarget;
          if (opt.children[0].className === "lml-select")
            opt.children[0].className = "lml-select active";
          else
            opt.children[0].className = "lml-select";
        });
      }
    }
    event(ev) {
      if (this.parent.type === "match") {

      } else {

      }
    }
  },

  p: class extends tagType {
    constructor() {
      super("p");
      this.childrenNames = ["string", "br", "command", "link", "b", "i", "u", "m"];
    }
    aHTML() {
      html.addTag(this, "p");
      this.html.lang = lml.course.meta.language;
    }
  },

  question: class extends tagType {
    constructor() {
      super("question");
      this.childrenNames = ["h1", "h2", "h3", "string", "br", "code", "p"];
    }
    aHTML() {
      html.addTag(this, "div");
    }
  },

  quiz: class extends tagType {
    constructor() {
      super("quiz");
      this.childrenNames = ["question", "q", "box", "text", "submit", "back"];
    }

    get check() {
      for (let elem of this.children) {
        if (elem.tagName === "box" || elem.tagName === "text") {
          if (!elem.check) {
            return false;
          }
        }
      }
      return true;
    }

    aHTML() {
      html.addTag(this, "div");
    }
  },

  seperator: class extends tagType {
    constructor() {
      super("seperator");
      this.tagInterupt = false;
    }
    aHTML() {
      html.addTag(this, "div");
    }
  },

  string: class extends tagType {
    constructor() {
      super("string");
    }
    aHTML() {
      html.addSpan(this);
    }
  },

  submit: class extends tagType {
    constructor() {
      super("submit");
      this.active = false;
    }

    next() {
      if (this.parent.html === V("#lml-lastMinified")) {
        lesson.next();
      } else {
        let lml = this.parent;
        while (lml.tagName !== "lml" && lml.parent !== undefined)
          lml = lml.parent;
        let node = lml.miniCache[this.parent.minifyNumber + 1];
        node.className = node.className.substr(0, node.className.length - 13);
        this.parent.html.className += " lml-minified";
      }
    }


    aHTML() {
      let htmlTag = d.cElem("p");
      htmlTag.className = "lml-submit";

      if (this.parent.tagName === "lesson" || V(".lml-quiz") === null)
        htmlTag.innerHTML = lml.course.meta.word.continue;
      else
        htmlTag.innerHTML = lml.course.meta.word.submit;

      this.html = this.parent.html.appendChild(htmlTag);


      if (this.parent.tagName === "lml") {
        this.html.cEv("click", (ev) => {
          this.blink();

          for (let elem of this.parent.children) {
            if (V(".lml-quiz") === null) {
              lesson.next();
            }
            if (elem.tagName === "quiz") {
              if (!elem.check) {
                if (this.html.innerText === lml.course.meta.word.again && V(".lml-skip") === null) {
                  let node = d.cElem("p");
                  node.innerText = lml.course.meta.word.skipLesson;
                  node.className = "lml-skip";
                  this.parent.html.appendChild(node);

                  node.aEv("click", () => {
                    lesson.next();
                  });
                }

                this.html.style.background = "var(--lml-darkBlue)";
                this.html.innerText = lml.course.meta.word.again;
                return;
              }
            }
          }

          this.html.rEv("click");

          this.html.style.background = "var(--lml-darkGreen)";
          this.html.innerHTML = lml.course.meta.word.continue;

          if (V(".lml-skip") !== null)
            this.parent.html.rChild(V(".lml-skip"));

          this.html.aEv("click", () => {
            lesson.next();
          });

        });

      } else {
        this.html.cEv("click", (ev) => {
          if (this.parent.tagName === "quiz") {
            if (!this.parent.check) {
              if (this.html.innerText === lml.course.meta.word.again && V(".lml-skip") === null) {
                let node = d.cElem("p");
                node.innerText = lml.course.meta.word.skip;
                node.className = "lml-skip";
                this.parent.html.appendChild(node);

                node.aEv("click", () => {
                  this.next();
                });
              }

              this.html.style.background = "var(--lml-darkBlue)";
              this.html.innerText = lml.course.meta.word.again;
              return;
            }
            this.html.rEv("click");

            this.html.style.background = "var(--lml-darkGreen)";
            this.html.innerHTML = lml.course.meta.word.continue;

            if (V(".lml-skip") !== null)
              this.parent.html.rChild(V(".lml-skip"));

            this.html.aEv("click", () => {
              this.next();
            });
            return;
          }

          this.html.style.background = "var(--lml-darkGreen)";
          this.html.innerHTML = lml.course.meta.word.continue;

          this.next();
        });
      }
    }

    blink() {
      if (!this.active) {

        let restore = () => {
          this.html.rEv("transitionend", restore);
          this.html.style.opacity = 1;
          let int = setInterval(() => {
            this.active = false;
            clearInterval(int);
          }, 30);
        };

        this.active = true;
        this.html.aEv("transitionend", restore);
        this.html.style.opacity = 0.7;
      }
    }
  },

  text: class extends tagType {
    constructor() {
      super("text");
      this.textSave = true;
      this.childrenNames = ["input", "string"];
    }

    get check() {
      return true;
    }

    parse_text(str) {
      this.children = [];
      str = cutEdges(str);

      let length = str.length,
        bracketStart = 0,
        bracketEnd = 0;

      for (let i = 0; i < length; i++) {
        if (bracketStart === 0) {
          if (str.charAt(i) === '[' || str.charAt(i) === '(') {
            if (str.charAt(i - 1) !== '\\') {
              bracketStart = i;
              this.aChild("string");
              this.lastChild.text = str.substr(bracketEnd + 1, bracketStart - bracketEnd - 1);
            }
          }
        } else {
          if (str.charAt(i) === ']' || str.charAt(i) === ')') {
            if (str.charAt(i - 1) !== '\\') {
              bracketEnd = i;
              this.aChild("input");
              this.lastChild.text = str.substr(bracketStart + 1, bracketEnd - bracketStart - 1);
              bracketStart = 0;
            }
          }
        }

        util.print(str.charAt(i))
      }
      if (bracketStart !== 0) {
        alert("failed");
        return;
      }
      if (bracketEnd !== length) {
        this.aChild("string");
        this.lastChild.text = str.substr(bracketEnd + 1);
      }
      //alert("'" + str + "'")
    }

    aHTML() {
      this.parse_text(this.lastChild.text);
      console.table(this.children);
      html.addTag(this, "div");
    }
  },

  u: class extends tagType {
    constructor() {
      super("u");
      this.childrenNames = ["string"];
      this.textSave = true;
    }
    aHTML() {
      html.addSpan(this, this.lastChild.text);
    }
  }
};

function new_node(tagName) {
  switch (tagName) {
    case "b":
      return new tag.b;
    case "back":
      return new tag.back;
    case "box":
      return new tag.box;
    case "br":
      return new tag.br;
    case "code":
      return new tag.code;
    case "command":
      return new tag.command;
    case "h1":
      return new tag.h1;
    case "h2":
      return new tag.h2;
    case "h3":
      return new tag.h3;
    case "i":
      return new tag.i;
    case "input":
      return new tag.input;
    case "image":
      return new tag.image;
    case "lesson":
      return new tag.lesson;
    case "link":
      return new tag.link;
    case "lml":
      return new tag.lml;
    case "m":
      return new tag.m;
    case "option":
      return new tag.option;
    case "p":
      return new tag.p;
    case "question":
      return new tag.question;
    case "quiz":
      return new tag.quiz;
    case "seperator":
      return new tag.seperator;
    case "string":
      return new tag.string;
    case "submit":
      return new tag.submit;
    case "text":
      return new tag.text;
    case "u":
      return new tag.u;
    default:
      console.error("unknown tag '" + tag + "'");
      return new tagType(tag);
  }
}

const parse = () => {

  const handleAttr = (str, obj) => {

    let strSize = str.length,
      mode = 0,
      begin,
      attrName;

    const setAttr = (attrValue) => {

      let i = -1,
        attr = obj[attrName];

      if (attr !== undefined) {

        if (attribs.bool.includes(attrName)) {
          if (attrValue.toLowerCase() === "true")
            obj[attrName] = true;
          else if (attrValue.toLowerCase() === "false")
            obj[attrName] = false;
          else
            console.error("Can't asign value '" + attrValue + "' to boolean attribute '" + attrName + "'");

        } else if (attribs.free.includes(attrName)) {
          obj[attrName] = attrValue;

        } else if (attribs.certain.forEach((value, index) => {
            if (value.name === attrName)
              i = index;
          }) === undefined && i !== -1) {

          if (attribs.certain[i].values.includes(attrValue.toLowerCase()))
            obj[attrName] = attrValue.toLowerCase();
          else
            console.error("Can't asign value '" + attrValue + "' to attribute '" + attrName + "'");

        } else {
          console.error("Attribute with name '" + attrName + "' doesn't exist");
        }
      } else {
        console.error("'" + obj.tagName + "' does't support attribute '" + attrName + "'");
      }
    };

    for (let i = 0; i < strSize; i++) {
      if (mode === 0 && (str.charAt(i) != ' ' && str.charAt(i) != '\n')) {
        begin = i;
        mode = 1;
      } else if (mode === 1 && str.charAt(i) === '=') {
        attrName = str.substr(begin, i - begin);
        mode = 2;
      } else if (mode === 2 && str.charAt(i) === '"') {
        begin = i;
        mode = 3;
      } else if (mode === 3 && str.charAt(i) === '"') {
        mode = 0;
        setAttr(str.substr(begin + 1, i - begin - 1));
      }
    }

    if (mode !== 0)
      console.error("invalid attribute '" + str + "' of '" + obj.tagName + "' ");
  }

  lml.root = new tag.root;

  let node = lml.root,
    saveMode = false,
    codeLength = lml.code.length,
    checkpoint = 0,
    insideTag = false;

  for (let i = 0; i < codeLength; i++) {

    if (saveMode === false) {
      if (insideTag === false && lml.code[i] === '<') {
        if (lml.code[i - 1] === '\\')
          continue;

        let text = lml.code.substr(checkpoint + 1, i - checkpoint - 1);

        if (/.*[^ \n].*/i.test(text)) { // regex to check if sting is empty
          node.aChild("string");
          node.lastChild.text = text;
        }

        checkpoint = i;
        insideTag = true;

      } else if (insideTag === true && lml.code[i] === '>') {
        let tagName;

        if (lml.code.charAt(checkpoint + 1) === '/') {
          tagName = lml.code.substr(checkpoint + 2, i - checkpoint - 2);

          if (node.tagName === tagName) {
            node = node.parent; // moving up again

          } else {
            console.error("'" + node.tagName + "' can not be interupted by '/" + tagName + "'");
            return;
          }

        } else {
          tagName = lml.code.substr(checkpoint + 1, i - checkpoint - 1);

          node.aChild(util.getTagName(tagName));

          let attrStr = tagName.substr(node.lastChild.tagName.length + 1, tagName.length - node.lastChild.tagName.length - 1);
          if (attrStr.length !== 0)
            handleAttr(attrStr, node.lastChild);

          if (node.lastChild.tagInterupt === true)
            node = node.lastChild;

          saveMode = node.textSave;
        }
        checkpoint = i;
        insideTag = false;
      }

    } else {

      if (lml.code[i] === '<') {
        if (lml.code[i - 1] === '\\')
          continue;

        let newLength = node.tagName.length + 2;

        if (length < i + newLength) {
          let expectedTag = "</" + node.tagName,
            newTag = lml.code.substr(i, newLength);

          if (newTag == expectedTag) {
            saveMode = false;
            insideTag = false;
            i--;
          }
        }
      }
    }
  }
  lml.root.aHTML();
}

lml.start = (start, cdn = "", path = "lml.json") => {

  lml.cdn = cdn;

  load.css("lml.css");
  load.css("lml-fonts.css");

  lml.htmlRoot = Q("#" + start);
  if (lml.htmlRoot === null) {
    lml.htmlRoot = Q("body");
  }

  load.document(path, (ev) => {

    lml.course = JSON.parse(ev.target.responseText);

    util.fixJson();

    if (lml.course.autoStart === true) {
      load.lesson(1, 1);
    } else {
      load.overview();
    }
  });
};
