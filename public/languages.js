/*
This file is based on:
  SHJS - Syntax Highlighting in JavaScript
  Copyright (C) 2007, 2008 gnombat@users.sourceforge.net

Copyright (C) 2018-2019 Aaron Erhardt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

languages['c'] = [
  [
    [/\/\/\//g, 'sh_comment', 1],
    [/\/\//g, 'sh_comment', 7],
    [/\/\*\*/g, 'sh_comment', 8],
    [/\/\*/g, 'sh_comment', 9],
    [/(\bstruct)([ \t]+)([A-Za-z0-9_]+)/g, ['sh_keyword', 'sh_normal', 'sh_classname'], -1],
    [/^[ \t]*#(?:[ \t]*include)/g, 'sh_preproc', 10, 1],
    [/^[ \t]*#(?:[ \t]*[A-Za-z0-9_]*)/g, 'sh_preproc', -1],
    [/\b[+-]?(?:(?:0x[A-Fa-f0-9]+)|(?:(?:[\d]*\.)?[\d]+(?:[eE][+-]?[\d]+)?))u?(?:(?:int(?:8|16|32|64))|L)?\b/g, 'sh_number', -1],
    [/"/g, 'sh_string', 13],
    [/'/g, 'sh_string', 14],
    [/\b(?:__asm|__cdecl|__declspec|__export|__far16|__fastcall|__fortran|__import|__pascal|__rtti|__stdcall|_asm|_cdecl|__except|_export|_far16|_fastcall|__finally|_fortran|_import|_pascal|_stdcall|__thread|__try|asm|auto|break|case|catch|cdecl|const|continue|default|do|else|enum|extern|for|goto|if|pascal|register|return|sizeof|static|struct|switch|typedef|union|volatile|while)\b/g, 'sh_keyword', -1],
    [/\b(?:bool|char|double|float|int|long|short|signed|unsigned|void|wchar_t)\b/g, 'sh_type', -1],
    [/~|!|%|\^|\*|\(|\)|-|\+|=|\[|\]|\\|:|;|,|\.|\/|\?|&|<|>|\|/g, 'sh_symbol', -1],
    [/\{|\}/g, 'sh_cbracket', -1],
    [/(?:[A-Za-z]|_)[A-Za-z0-9_]*(?=[ \t]*\()/g, 'sh_function', -1],
    [/([A-Za-z](?:[^`~!@#$%&*()_=+{}|;:",<.>\/?'\\[\]\^\-\s]|[_])*)((?:<.*>)?)(\s+(?=[*&]*[A-Za-z][^`~!@#$%&*()_=+{}|;:",<.>\/?'\\[\]\^\-\s]*\s*[`~!@#$%&*()_=+{}|;:",<.>\/?'\\[\]\^\-\[\]]+))/g, ['sh_usertype', 'sh_usertype', 'sh_normal'], -1]
  ],
  [
    [/$/g, null, -2],
    [/(?:<?)[A-Za-z0-9_\.\/\-_~]+@[A-Za-z0-9_\.\/\-_~]+(?:>?)|(?:<?)[A-Za-z0-9_]+:\/\/[A-Za-z0-9_\.\/\-_~]+(?:>?)/g, 'sh_url', -1],
    [/<\?xml/g, 'sh_preproc', 2, 1],
    [/<!DOCTYPE/g, 'sh_preproc', 4, 1],
    [/<!--/g, 'sh_comment', 5],
    [/<(?:\/)?[A-Za-z](?:[A-Za-z0-9_:.-]*)(?:\/)?>/g, 'sh_keyword', -1],
    [/<(?:\/)?[A-Za-z](?:[A-Za-z0-9_:.-]*)/g, 'sh_keyword', 6, 1],
    [/&(?:[A-Za-z0-9]+);/g, 'sh_preproc', -1],
    [/<(?:\/)?[A-Za-z][A-Za-z0-9]*(?:\/)?>/g, 'sh_keyword', -1],
    [/<(?:\/)?[A-Za-z][A-Za-z0-9]*/g, 'sh_keyword', 6, 1],
    [/@[A-Za-z]+/g, 'sh_type', -1],
    [/(?:TODO|FIXME|BUG)(?:[:]?)/g, 'sh_todo', -1]
  ],
  [
    [/\?>/g, 'sh_preproc', -2],
    [/([^=" \t>]+)([ \t]*)(=?)/g, ['sh_type', 'sh_normal', 'sh_symbol'], -1],
    [/"/g, 'sh_string', 3]
  ],
  [
    [/\\(?:\\|")/g, null, -1],
    [/"/g, 'sh_string', -2]
  ],
  [
    [/>/g, 'sh_preproc', -2],
    [/([^=" \t>]+)([ \t]*)(=?)/g, ['sh_type', 'sh_normal', 'sh_symbol'], -1],
    [/"/g, 'sh_string', 3]
  ],
  [
    [/-->/g, 'sh_comment', -2],
    [/<!--/g, 'sh_comment', 5]
  ],
  [
    [/(?:\/)?>/g, 'sh_keyword', -2],
    [/([^=" \t>]+)([ \t]*)(=?)/g, ['sh_type', 'sh_normal', 'sh_symbol'], -1],
    [/"/g, 'sh_string', 3]
  ],
  [
    [/$/g, null, -2]
  ],
  [
    [/\*\//g, 'sh_comment', -2],
    [/(?:<?)[A-Za-z0-9_\.\/\-_~]+@[A-Za-z0-9_\.\/\-_~]+(?:>?)|(?:<?)[A-Za-z0-9_]+:\/\/[A-Za-z0-9_\.\/\-_~]+(?:>?)/g, 'sh_url', -1],
    [/<\?xml/g, 'sh_preproc', 2, 1],
    [/<!DOCTYPE/g, 'sh_preproc', 4, 1],
    [/<!--/g, 'sh_comment', 5],
    [/<(?:\/)?[A-Za-z](?:[A-Za-z0-9_:.-]*)(?:\/)?>/g, 'sh_keyword', -1],
    [/<(?:\/)?[A-Za-z](?:[A-Za-z0-9_:.-]*)/g, 'sh_keyword', 6, 1],
    [/&(?:[A-Za-z0-9]+);/g, 'sh_preproc', -1],
    [/<(?:\/)?[A-Za-z][A-Za-z0-9]*(?:\/)?>/g, 'sh_keyword', -1],
    [/<(?:\/)?[A-Za-z][A-Za-z0-9]*/g, 'sh_keyword', 6, 1],
    [/@[A-Za-z]+/g, 'sh_type', -1],
    [/(?:TODO|FIXME|BUG)(?:[:]?)/g, 'sh_todo', -1]
  ],
  [
    [/\*\//g, 'sh_comment', -2],
    [/(?:<?)[A-Za-z0-9_\.\/\-_~]+@[A-Za-z0-9_\.\/\-_~]+(?:>?)|(?:<?)[A-Za-z0-9_]+:\/\/[A-Za-z0-9_\.\/\-_~]+(?:>?)/g, 'sh_url', -1],
    [/(?:TODO|FIXME|BUG)(?:[:]?)/g, 'sh_todo', -1]
  ],
  [
    [/$/g, null, -2],
    [/</g, 'sh_string', 11],
    [/"/g, 'sh_string', 12],
    [/\/\/\//g, 'sh_comment', 1],
    [/\/\//g, 'sh_comment', 7],
    [/\/\*\*/g, 'sh_comment', 8],
    [/\/\*/g, 'sh_comment', 9]
  ],
  [
    [/$/g, null, -2],
    [/>/g, 'sh_string', -2]
  ],
  [
    [/$/g, null, -2],
    [/\\(?:\\|")/g, null, -1],
    [/"/g, 'sh_string', -2]
  ],
  [
    [/"/g, 'sh_string', -2],
    [/\\./g, 'sh_specialchar', -1]
  ],
  [
    [/'/g, 'sh_string', -2],
    [/\\./g, 'sh_specialchar', -1]
  ]
];