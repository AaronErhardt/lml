/*
This file is based on:
  SHJS - Syntax Highlighting in JavaScript
  Copyright (C) 2007, 2008 gnombat@users.sourceforge.net

Copyright (C) 2018-2019 Aaron Erhardt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

"use strict";
let languages = {};

function highlight(str, language, html) {
  let out = highlightString(str, languages[language]);
  out = insertTags(out, str);

  while (html.hasChildNodes()) {
    html.removeChild(html.firstChild);
  }
  html.appendChild(out);
}

function insertTags(tags, text) {
  let doc = document;
  let result = document.createDocumentFragment();
  let tagIndex = 0;
  let numTags = tags.length;
  let textPos = 0;
  let textLength = text.length;
  let currentNode = result; // output one tag or text node every iteration

  while (textPos < textLength || tagIndex < numTags) {
    let tag;
    let tagPos;

    if (tagIndex < numTags) {
      tag = tags[tagIndex];
      tagPos = tag.pos;

    } else {
      tagPos = textLength;
    }

    if (tagPos <= textPos) { // output the tag

      if (tag.node) { // start tag
        let newNode = tag.node;
        currentNode.appendChild(newNode);
        currentNode = newNode;

      } else { // end tag
        currentNode = currentNode.parentNode;
      }
      tagIndex++;

    } else { // output text
      currentNode.appendChild(doc.createTextNode(text.substring(textPos, tagPos)));
      textPos = tagPos;
    }
  }
  return result;
}

function highlightString(inputString, language) {
  let a = document.createElement('a');
  let span = document.createElement('span'); // the result
  let tags = [];
  let numTags = 0; // each element is a pattern object from language
  let patternStack = []; // the current position within inputString
  let pos = 0; // the name of the current style, or null if there is no current style
  let currentStyle = null;
  let output = (s, style) => {
    let length = s.length; // this is more than just an optimization - we don't want to output empty <span></span> elements
    if (length === 0) {
      return;
    }
    if (!style) {
      let stackLength = patternStack.length;
      if (stackLength !== 0) {
        let pattern = patternStack[stackLength - 1]; // check whether this is a state or an environment
        if (!pattern[3]) { // it's not a state - it's an environment; use the style for this environment
          style = pattern[1];
        }
      }
    }
    if (currentStyle !== style) {
      if (currentStyle) {
        tags[numTags++] = {
          pos: pos
        };
        if (currentStyle === 'sh_url') {
          sh_setHref(tags, numTags, inputString);
        }
      }
      if (style) {
        let clone;
        if (style === 'sh_url') {
          clone = a.cloneNode(false);
        } else {
          clone = span.cloneNode(false);
        }
        clone.className = style;
        tags[numTags++] = {
          node: clone,
          pos: pos
        };
      }
    }
    pos += length;
    currentStyle = style;
  };

  let endOfLinePattern = /\r\n|\r|\n/g;
  endOfLinePattern.lastIndex = 0;
  let inputStringLength = inputString.length;

  while (pos < inputStringLength) {
    let start = pos;
    let end;
    let startOfNextLine;
    let endOfLineMatch = endOfLinePattern.exec(inputString);

    if (endOfLineMatch === null) {
      end = inputStringLength;
      startOfNextLine = inputStringLength;

    } else {
      end = endOfLineMatch.index;
      startOfNextLine = endOfLinePattern.lastIndex;
    }

    let line = inputString.substring(start, end);
    let matchCache = [];

    for (;;) {
      let posWithinLine = pos - start;
      let stateIndex;
      let stackLength = patternStack.length;

      if (stackLength === 0) {
        stateIndex = 0;

      } else { // get the next state
        stateIndex = patternStack[stackLength - 1][2];
      }

      let state = language[stateIndex];
      let numPatterns = state.length;
      let mc = matchCache[stateIndex];

      if (!mc) {
        mc = matchCache[stateIndex] = [];
      }
      let bestMatch = null;
      let bestPatternIndex = -1;

      for (let i = 0; i < numPatterns; i++) {
        let match;

        if (i < mc.length && (mc[i] === null || posWithinLine <= mc[i].index)) {
          match = mc[i];

        } else {
          let regex = state[i][0];
          regex.lastIndex = posWithinLine;
          match = regex.exec(line);
          mc[i] = match;
        }

        if (match !== null && (bestMatch === null || match.index < bestMatch.index)) {
          bestMatch = match;
          bestPatternIndex = i;

          if (match.index === posWithinLine) {
            break;
          }
        }
      }

      if (bestMatch === null) {
        output(line.substring(posWithinLine), null);
        break;

      } else { // got a match
        if (bestMatch.index > posWithinLine) {
          output(line.substring(posWithinLine, bestMatch.index), null);
        }

        let pattern = state[bestPatternIndex];
        let newStyle = pattern[1];
        let matchedString;

        if (newStyle instanceof Array) {
          for (let subexpression = 0; subexpression < newStyle.length; subexpression++) {
            matchedString = bestMatch[subexpression + 1];
            output(matchedString, newStyle[subexpression]);
          }

        } else {
          matchedString = bestMatch[0];
          output(matchedString, newStyle);
        }

        switch (pattern[2]) {
          case -1: // do nothing
            break;
          case -2: // exit
            patternStack.pop();
            break;
          case -3: // exitall
            patternStack.length = 0;
            break;
          default: // this was the start of a delimited pattern or a state/environment
            patternStack.push(pattern);
            break;
        }
      }
    } // end of the line

    if (currentStyle) {
      tags[numTags++] = {
        pos: pos
      };

      if (currentStyle === 'sh_url') {
        sh_setHref(tags, numTags, inputString);
      }

      currentStyle = null;
    }
    pos = startOfNextLine;
  }

  return tags;
}

function sh_isEmailAddress(url) {
  if (/^mailto:/.test(url)) {
    return false;
  }
  return url.indexOf('@') !== -1;
}

function sh_setHref(tags, numTags, inputString) {
  var url = inputString.substring(tags[numTags - 2].pos, tags[numTags - 1].pos);

  if (url.length >= 2 && url.charAt(0) === '<' && url.charAt(url.length - 1) === '>') {
    url = url.substr(1, url.length - 2);
  }

  if (sh_isEmailAddress(url)) {
    url = 'mailto:' + url;
  }
  tags[numTags - 2].node.href = url;
}

languages['arduino'] = [
  [
    [/(\b(?:class|struct|typename))([ \t]+)([A-Za-z0-9_]+)/g, ['sh_keyword', 'sh_normal', 'sh_classname'], -1],
    [/\b(?:class|const_cast|delete|dynamic_cast|explicit|false|friend|inline|mutable|namespace|new|operator|private|protected|public|reinterpret_cast|static_cast|template|this|throw|true|try|typeid|typename|using|virtual)\b/g, 'sh_keyword', -1],
    [/\b(?:digitalRead|digitalWrite|pinMode|analogRead|analogReference|analogWrite|analogReadResolution|analogWriteResolution|noTone|pulseIn|pulseInLong|shiftIn|shiftOut|tone|delay|delayMicroseconds|micros|millis|abs|constrain|map|max|min|pow|sq|sqrt|cos|sin|tan|isAlpha|isAlphaNumeric|isAscii|isControl|isDigit|isGraph|isHexadecimalDigit|isLowerCase|isPrintable|isPunct|isSpace|isUpperCase|isWhitespace|random|randomSeed|bit|bitClear|bitRead|bitSet|bitWrite|highByte|lowByte|attachInterrupt|detachInterrupt|Interrupts|interrupts|noInterrupts|Serial|stream|Keyboard|Mouse)\b/g, 'sh_arduino_keyword', -1],
    [/\/\/\//g, 'sh_single_line_comment', 1],
    [/\/\//g, 'sh_single_line_comment', 7],
    [/\/\*\*/g, 'sh_multi_line_comment', 8],
    [/\/\*/g, 'sh_multi_line_comment', 9],
    [/(\bstruct)([ \t]+)([A-Za-z0-9_]+)/g, ['sh_keyword', 'sh_normal', 'sh_classname'], -1],
    [/^[ \t]*#(?:[ \t]*include)/g, 'sh_preproc', 10, 1],
    [/^[ \t]*#(?:[ \t]*[A-Za-z0-9_]*)/g, 'sh_preproc', -1],
    [/\b[+-]?(?:(?:0x[A-Fa-f0-9]+)|(?:(?:[\d]*\.)?[\d]+(?:[eE][+-]?[\d]+)?))u?(?:(?:int(?:8|16|32|64))|L)?\b/g, 'sh_number', -1],
    [/"/g, 'sh_string', 13],
    [/'/g, 'sh_string', 14],
    [/\b(?:__asm|__cdecl|__declspec|__export|__far16|__fastcall|__fortran|__import|__pascal|__rtti|__stdcall|_asm|_cdecl|__except|_export|_far16|_fastcall|__finally|_fortran|_import|_pascal|_stdcall|__thread|__try|asm|auto|break|case|catch|cdecl|continue|default|do|else|enum|extern|for|goto|if|pascal|register|return|sizeof|static|struct|switch|typedef|union|volatile|while|setup|loop)\b/g, 'sh_keyword', -1],
    [/\b(?:bool|char|double|float|int|long|short|signed|unsigned|void|wchar_t|const)\b/g, 'sh_type', -1],
    [/\(|\)|\[|\]|\\|:|;|,|\.|\?/g, 'sh_symbol', -1],
    [/~|=|<|>|-|\+|&|\*|!|%|\^|\/|\|/g, 'sh_operator', -1],
    [/\{|\}/g, 'sh_cbracket', -1],
    [/(?:[A-Za-z]|_)[A-Za-z0-9_]*(?=[ \t]*\()/g, 'sh_function', -1],
    [/([A-Za-z](?:[^`~!@#$%&*()_=+{}|;:",<.>\/?'\\[\]\^\-\s]|[_])*)((?:<.*>)?)(\s+(?=[*&]*[A-Za-z][^`~!@#$%&*()_=+{}|;:",<.>\/?'\\[\]\^\-\s]*\s*[`~!@#$%&*()_=+{}|;:",<.>\/?'\\[\]\^\-\[\]]+))/g, ['sh_usertype', 'sh_usertype', 'sh_normal'], -1]
  ],
  [
    [/$/g, null, -2],
    [/(?:<?)[A-Za-z0-9_\.\/\-_~]+@[A-Za-z0-9_\.\/\-_~]+(?:>?)|(?:<?)[A-Za-z0-9_]+:\/\/[A-Za-z0-9_\.\/\-_~]+(?:>?)/g, 'sh_url', -1],
    [/<\?xml/g, 'sh_preproc', 2, 1],
    [/<!DOCTYPE/g, 'sh_preproc', 4, 1],
    [/<!--/g, 'sh_multi_line_comment', 5],
    [/<(?:\/)?[A-Za-z](?:[A-Za-z0-9_:.-]*)(?:\/)?>/g, 'sh_keyword', -1],
    [/<(?:\/)?[A-Za-z](?:[A-Za-z0-9_:.-]*)/g, 'sh_keyword', 6, 1],
    [/&(?:[A-Za-z0-9]+);/g, 'sh_preproc', -1],
    [/<(?:\/)?[A-Za-z][A-Za-z0-9]*(?:\/)?>/g, 'sh_keyword', -1],
    [/<(?:\/)?[A-Za-z][A-Za-z0-9]*/g, 'sh_keyword', 6, 1],
    [/@[A-Za-z]+/g, 'sh_type', -1],
    [/(?:TODO|FIXME|BUG)(?:[:]?)/g, 'sh_todo', -1]
  ],
  [
    [/\?>/g, 'sh_preproc', -2],
    [/([^=" \t>]+)([ \t]*)(=?)/g, ['sh_type', 'sh_normal', 'sh_symbol'], -1],
    [/"/g, 'sh_string', 3]
  ],
  [
    [/\\(?:\\|")/g, null, -1],
    [/"/g, 'sh_string', -2]
  ],
  [
    [/>/g, 'sh_preproc', -2],
    [/([^=" \t>]+)([ \t]*)(=?)/g, ['sh_type', 'sh_normal', 'sh_symbol'], -1],
    [/"/g, 'sh_string', 3]
  ],
  [
    [/-->/g, 'sh_multi_line_comment', -2],
    [/<!--/g, 'sh_multi_line_comment', 5]
  ],
  [
    [/(?:\/)?>/g, 'sh_keyword', -2],
    [/([^=" \t>]+)([ \t]*)(=?)/g, ['sh_type', 'sh_normal', 'sh_symbol'], -1],
    [/"/g, 'sh_string', 3]
  ],
  [
    [/$/g, null, -2]
  ],
  [
    [/\*\//g, 'sh_multi_line_comment', -2],
    [/(?:<?)[A-Za-z0-9_\.\/\-_~]+@[A-Za-z0-9_\.\/\-_~]+(?:>?)|(?:<?)[A-Za-z0-9_]+:\/\/[A-Za-z0-9_\.\/\-_~]+(?:>?)/g, 'sh_url', -1],
    [/<(?:\/)?[A-Za-z](?:[A-Za-z0-9_:.-]*)(?:\/)?>/g, 'sh_keyword', -1],
    [/<(?:\/)?[A-Za-z](?:[A-Za-z0-9_:.-]*)/g, 'sh_keyword', 6, 1],
    [/&(?:[A-Za-z0-9]+);/g, 'sh_preproc', -1],
    [/<(?:\/)?[A-Za-z][A-Za-z0-9]*(?:\/)?>/g, 'sh_keyword', -1],
    [/<(?:\/)?[A-Za-z][A-Za-z0-9]*/g, 'sh_keyword', 6, 1],
    [/@[A-Za-z]+/g, 'sh_type', -1],
    [/(?:TODO|FIXME|BUG)(?:[:]?)/g, 'sh_todo', -1]
  ],
  [
    [/\*\//g, 'sh_multi_line_comment', -2],
    [/(?:<?)[A-Za-z0-9_\.\/\-_~]+@[A-Za-z0-9_\.\/\-_~]+(?:>?)|(?:<?)[A-Za-z0-9_]+:\/\/[A-Za-z0-9_\.\/\-_~]+(?:>?)/g, 'sh_url', -1],
    [/(?:TODO|FIXME|BUG)(?:[:]?)/g, 'sh_todo', -1]
  ],
  [
    [/$/g, null, -2],
    [/</g, 'sh_string', 11],
    [/"/g, 'sh_string', 12],
    [/\/\/\//g, 'sh_single_line_comment', 1],
    [/\/\//g, 'sh_single_line_comment', 7],
    [/\/\*\*/g, 'sh_multi_line_comment', 8],
    [/\/\*/g, 'sh_multi_line_comment', 9]
  ],
  [
    [/$/g, null, -2],
    [/>/g, 'sh_string', -2]
  ],
  [
    [/$/g, null, -2],
    [/\\(?:\\|")/g, null, -1],
    [/"/g, 'sh_string', -2]
  ],
  [
    [/"/g, 'sh_string', -2],
    [/\\./g, 'sh_specialchar', -1]
  ],
  [
    [/'/g, 'sh_string', -2],
    [/\\./g, 'sh_specialchar', -1]
  ]
];