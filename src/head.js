/*
Copyright (C) 2018-2019 Aaron Erhardt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

"use strict"

/* Check for incompatible browsers*/

try {
  let test = a => ++a;
} catch (e) {
  alert("Your browser is not supported anymore. We recommend to use the latest version of Firefox!");
}

/* Global variables */

const lml = {
  chapter: 1,
  lesson: 1,
  code: "",
  root: {},
  htmlRoot: {},
  course: {},
  cdn: "",
  mobile: navigator.userAgent.indexOf("mobile") !== -1
};

const attribs = {
  bool: ["br", "correct", "hide"],
  certain: [{
      name: "lang",
      values: ["c", "arduino"]
    },
    {
      name: "type",
      values: ["select", "sort", "match"]
    },
  ],
  free: ["src", "url"]
};
