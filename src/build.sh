#/bin/bash

dir=`dirname $0`

cd $dir

function addFile {
  tail -n +17 $1 >> ../public/lml.js
}

cat head.js > ../public/lml.js

addFile qute.js
addFile load.js
addFile utility.js
addFile objects.js
addFile parser.js
addFile core.js
