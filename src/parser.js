/*
Copyright (C) 2019 Aaron Erhardt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const parse = () => {

  const handleAttr = (str, obj) => {

    let strSize = str.length,
      mode = 0,
      begin,
      attrName;

    const setAttr = (attrValue) => {

      let i = -1,
        attr = obj[attrName];

      if (attr !== undefined) {

        if (attribs.bool.includes(attrName)) {
          if (attrValue.toLowerCase() === "true")
            obj[attrName] = true;
          else if (attrValue.toLowerCase() === "false")
            obj[attrName] = false;
          else
            console.error("Can't asign value '" + attrValue + "' to boolean attribute '" + attrName + "'");

        } else if (attribs.free.includes(attrName)) {
          obj[attrName] = attrValue;

        } else if (attribs.certain.forEach((value, index) => {
            if (value.name === attrName)
              i = index;
          }) === undefined && i !== -1) {

          if (attribs.certain[i].values.includes(attrValue.toLowerCase()))
            obj[attrName] = attrValue.toLowerCase();
          else
            console.error("Can't asign value '" + attrValue + "' to attribute '" + attrName + "'");

        } else {
          console.error("Attribute with name '" + attrName + "' doesn't exist");
        }
      } else {
        console.error("'" + obj.tagName + "' does't support attribute '" + attrName + "'");
      }
    };

    for (let i = 0; i < strSize; i++) {
      if (mode === 0 && (str.charAt(i) != ' ' && str.charAt(i) != '\n')) {
        begin = i;
        mode = 1;
      } else if (mode === 1 && str.charAt(i) === '=') {
        attrName = str.substr(begin, i - begin);
        mode = 2;
      } else if (mode === 2 && str.charAt(i) === '"') {
        begin = i;
        mode = 3;
      } else if (mode === 3 && str.charAt(i) === '"') {
        mode = 0;
        setAttr(str.substr(begin + 1, i - begin - 1));
      }
    }

    if (mode !== 0)
      console.error("invalid attribute '" + str + "' of '" + obj.tagName + "' ");
  }

  lml.root = new tag.root;

  let node = lml.root,
    saveMode = false,
    codeLength = lml.code.length,
    checkpoint = 0,
    insideTag = false;

  for (let i = 0; i < codeLength; i++) {

    if (saveMode === false) {
      if (insideTag === false && lml.code[i] === '<') {
        if (lml.code[i - 1] === '\\')
          continue;

        let text = lml.code.substr(checkpoint + 1, i - checkpoint - 1);

        if (/.*[^ \n].*/i.test(text)) { // regex to check if sting is empty
          node.aChild("string");
          node.lastChild.text = text;
        }

        checkpoint = i;
        insideTag = true;

      } else if (insideTag === true && lml.code[i] === '>') {
        let tagName;

        if (lml.code.charAt(checkpoint + 1) === '/') {
          tagName = lml.code.substr(checkpoint + 2, i - checkpoint - 2);

          if (node.tagName === tagName) {
            node = node.parent; // moving up again

          } else {
            console.error("'" + node.tagName + "' can not be interupted by '/" + tagName + "'");
            return;
          }

        } else {
          tagName = lml.code.substr(checkpoint + 1, i - checkpoint - 1);

          node.aChild(util.getTagName(tagName));

          let attrStr = tagName.substr(node.lastChild.tagName.length + 1, tagName.length - node.lastChild.tagName.length - 1);
          if (attrStr.length !== 0)
            handleAttr(attrStr, node.lastChild);

          if (node.lastChild.tagInterupt === true)
            node = node.lastChild;

          saveMode = node.textSave;
        }
        checkpoint = i;
        insideTag = false;
      }

    } else {

      if (lml.code[i] === '<') {
        if (lml.code[i - 1] === '\\')
          continue;

        let newLength = node.tagName.length + 2;

        if (length < i + newLength) {
          let expectedTag = "</" + node.tagName,
            newTag = lml.code.substr(i, newLength);

          if (newTag == expectedTag) {
            saveMode = false;
            insideTag = false;
            i--;
          }
        }
      }
    }
  }
  lml.root.aHTML();
}
