/*
Copyright (C) 2019 Aaron Erhardt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const util = {
  fixJson() {
    if (lml.course.folderName === undefined)
      lml.course.folderName = "";
    if (lml.course.chapterName === undefined)
      lml.course.chapterName = "CHAPTER_";
    if (lml.course.lessonName === undefined)
      lml.course.lessonName = "LESSON_";

    if (lml.course.meta === undefined)
      lml.course.meta = {};
    if (lml.course.meta.word === undefined)
      lml.course.meta.word = {};
    if (lml.course.meta.lang === undefined)
      lml.course.meta.lang = "en";
    if (lml.course.meta.word.back === undefined)
      lml.course.meta.word.back = "go back";
    if (lml.course.meta.word.submit === undefined)
      lml.course.meta.word.submit = "submit";
    if (lml.course.meta.word.continue === undefined)
      lml.course.meta.word.continue = "continue";
    if (lml.course.meta.word.skip === undefined)
      lml.course.meta.word.skip = "skip";
    if (lml.course.meta.word.skipLesson === undefined)
      lml.course.meta.word.skipLesson = "skip lesson";
    if (lml.course.meta.word.again === undefined)
      lml.course.meta.word.again = "try again";
  },

  clear: () => {
    if (lml.htmlRoot.firstChild !== null)
      lml.htmlRoot.rChild(lml.htmlRoot.firstChild);
  },

  getTagName: (tag) => {
    let tagNameSize = tag.length;
    for (let i = 0; i < tagNameSize; i++) {
      if (tag.charAt(i) === " ")
        return tag.substr(0, i);
    }
    return tag;
  },

  cutEdges: (str) => {
    let length = str.length;
    let begin = 0,
      end = length;

    for (let i = 0; i < length; i++) {
      let char = str.charAt(i);
      if (char !== ' ' && char !== '\n') {
        begin = i;
        break;
      }
    }

    for (let i = length - 1; i >= 0; i--) {
      let char = str.charAt(i);
      if (char !== ' ' && char !== '\n') {
        end = i + 1;
        break;
      }
    }
    return str.substr(begin, end - begin);
  },

  print: (str) => {
    if (lml.course.showLog === true)
      console.log(str);
  }
};

const lesson = {
  next: () => {
    if (lml.course.chapter[lml.chapter - 1].lesson.length === lml.lesson)
      load.overview();
    else
      load.lesson(lml.chapter, lml.lesson + 1);
  },

  last: () => {
    if (lml.lesson <= 1)
      load.overview();
    else
      load.lesson(lml.chapter, lml.lesson - 1);
  }
};

const html = {
  addTag: (obj, tagName) => {
    let htmlTag = d.cElem(tagName);
    htmlTag.className = "lml-" + obj.tagName;
    obj.html = obj.parent.html.appendChild(htmlTag);

    for (let elem of obj.children) {
      elem.aHTML();
    }
  },

  addSpan: (obj, str) => {
    let htmlTag = d.cElem("span");
    let t_node;
    if (str === undefined) {
      t_node = document.createTextNode(obj.text);
    } else {
      t_node = document.createTextNode(str);
    }
    obj.html = obj.parent.html.appendChild(htmlTag);
    obj.html.className = "lml-" + obj.tagName;
    obj.html.appendChild(t_node);
  }
};
