/*
Copyright (C) 2019 Aaron Erhardt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const load = {

  lesson: (chapter, lesson) => {
    let path = lml.course.folderName + lml.course.chapterName + chapter + "/" + lml.course.lessonName + lesson + ".lml";
    lml.chapter = chapter;
    lml.lesson = lesson;

    let func = (ev) => {
      lml.code = ev.target.responseText;

      util.clear();
      parse();
    }

    load.document(path, func);
    w.scrollTo(0, 0);
  },

  overview: () => {
    util.clear();

    let node = lml.htmlRoot,
      chapterNum = 1;

    let html = d.cElem("div");
    html.className = "lml-overview";
    node = node.appendChild(html);

    html = d.cElem("h");
    html.textContent = lml.course.name;
    html.className = "lml-course-name";
    node.aChild(html);

    html = d.cElem("p");
    html.textContent = lml.course.description;
    html.className = "lml-course-description";
    node.aChild(html);

    for (let chapter of lml.course.chapter) {
      html = d.cElem("div");
      html.className = "lml-chapter";
      let cNode = node.aChild(html);

      let evChapter = chapterNum;
      cNode.aEv("click", () => {
        load.lesson(evChapter, 1);
      });

      html = d.cElem("h");
      html.textContent = chapter.name;
      html.className = "lml-chapter-name";
      cNode.aChild(html);

      html = d.cElem("p");
      html.textContent = chapter.description;
      html.className = "lml-chapter-description";
      cNode.aChild(html);

      ++chapterNum;
    }
    w.scrollTo(0, 0);
  },

  document: (path, func) => {
    util.print("opening document at " + path);

    let client = new XMLHttpRequest();
    client.responseType = "text";
    client.open('GET', path, true);

    let evFunc = (ev) => {
      if (ev.target.readyState === 4) {
        if (ev.target.status === 200) {
          func(ev);
        } else {
          console.error("couldn't open document at '" + path + "' (" + ev.target.status + ")");
        }
      }
    };
    client.addEventListener("readystatechange", evFunc);
    client.send();
  },

  css: (path) => {
    let head = document.getElementsByTagName("head")[0];
    let link = d.cElem("link");
    link.rel = "stylesheet";
    link.type = "text/css";
    link.href = lml.cdn + path;
    head.appendChild(link);
  }
};
