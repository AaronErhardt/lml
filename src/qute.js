/*
Copyright (C) 2019 Aaron Erhardt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const qObj = (o) => {

  if (o === undefined || o === null) {
    util.print("DEBUG: Unknown object!");
    return undefined;
  }

  if (o.ev !== undefined) {
    return o;
  }

  o.ev = [];

  o.aEv = (ev, func, capt = false) => o.addEventListener(ev, func, capt);

  o.rEv = (ev, func, capt = false) => {

    if (ev === undefined) {
      for (let i = o.ev.length - 1; i >= 0; i--) {
        o.removeEventListener(o.ev[i].name, o.ev[i].func, o.ev[i].capt);
      }
      o.ev = [];

    } else if (func === undefined) {

      for (let i = o.ev.length - 1; i >= 0; i--) {
        if (ev === o.ev[i].name) {
          o.removeEventListener(ev, o.ev[i].func, o.ev[i].capt);
          o.ev.splice(i, 1);
          return;
        }
      }

    } else if (typeof(func) === "number") {
      o.removeEventListener(ev, o.ev[func], ev[func].capt);
      o.ev.splice(func, 1);

    } else {

      o.removeEventListener(ev, func, capt);
    }
  };

  o.cEv = (ev, func, capt = false) => {
    o.ev.push({
      name: ev,
      func: func,
      capt: capt
    });
    o.addEventListener(ev, func, capt);
  };

  //Element queries\\

  o.Q = (q) => {
    return qObj(o.querySelector(q));
  };

  o.A = (q) => {
    let t = o.querySelectorAll(q);
    let c = [];

    for (let i = t.length - 1; i >= 0; i--) {
      c[i] = qObj(t[i]);
    }
    return c;
  };

  //DOM operations\\

  o.aChild = (c) => {
    return qObj(o.appendChild(c));
  };

  o.rChild = (c) => {
    return o.removeChild(c);
  };

  return o;
};

//vanilla

const w = qObj(window);

const d = qObj(document);

d.cElem = (e, q = true) => {
  return qObj(d.createElement(e));
};

d.cText = (t) => {
  return d.createTextNode(t);
};

const V = (q) => {
  return d.querySelector(q);
};

const Y = (q) => {
  return d.querySelectorAll(q);
};

//Qute

const Q = (q) => {
  return qObj(d.querySelector(q));
};

const A = (q) => {
  let t = d.querySelectorAll(q);
  let c = [];

  for (let i = t.length - 1; i >= 0; i--) {
    c[i] = qObj(t[i]);
  }
  return c;
};
