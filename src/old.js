/*
Copyright (C) 2019 Aaron Erhardt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// "old" code (might still be useful in the future)

/*function preprocess() {
  let begin = 0,
    end = 0,
    codeLength = LMLCode.length;

  for (let i = 0; i < codeLength; i++) {
    if (LMLCode.charAt(i) == '<' && LMLCode.substr(i + 1, 3) === "lml") {
      begin = i;
      break;
    }
  }
  for (let i = codeLength; i > begin; i--) {
    if (LMLCode.charAt(i) == '<' && LMLCode.substr(i + 1, 4) === "/lml") {
      end = i;
      break;
    }
  }
  LMLCode = LMLCode.substr(begin, end - begin);
}*/

/*function lineBreak(obj, pos) {
  let str = obj.children[pos].text;
  str = shorten_edges(str);
  str = str.split(' ').join('\u00a0');
  let array = [];
  for (let i = str.indexOf("\n"); i !== -1; i = str.indexOf("\n")) {
    array.push(str.substr(0, i));
    str = str.substr(i + 1);
  }
  let ret = array.length;
  array.push(str);
  if (array === []) return;
  obj.children[pos].text = array[array.length - 1];
  for (let i = array.length - 2; i >= 0; i--) {
    let node = new string;
    obj.children.splice(pos, 0, node);
    node.text = array[i];
    array.pop();
  }
  return ret;
}*/
