/*
Copyright (C) 2019 Aaron Erhardt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

lml.start = (start, cdn = "", path = "lml.json") => {

  lml.cdn = cdn;

  load.css("lml.css");
  load.css("lml-fonts.css");

  lml.htmlRoot = Q("#" + start);
  if (lml.htmlRoot === null) {
    lml.htmlRoot = Q("body");
  }

  load.document(path, (ev) => {

    lml.course = JSON.parse(ev.target.responseText);

    util.fixJson();

    if (lml.course.autoStart === true) {
      load.lesson(1, 1);
    } else {
      load.overview();
    }
  });
};
